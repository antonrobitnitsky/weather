/* Core */
import { useState, useEffect } from 'react';

/* Mock data */
import days from '../mock-data/forecast.json';

export const Forecast = () => {
    const getDay = timeStamp => new Date(timeStamp).toLocaleDateString('ru', { weekday: 'long' });;
    
    const daysJSX = days.slice(0, 7).map((day, index) => {
        return (
            <div className={ `day ${ day.type } ${ index === 0 ? 'selected' : '' }` }>
                <p>{ getDay(day.day) }</p>
                <span>{ day.temperature }</span>
            </div>
        );
    });
    
    return (
        <>
            <div className="forecast">
                { daysJSX }
            </div>
        </>
    );
}