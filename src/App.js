/* Core */
import { Router, Route } from 'react-router-dom';

/* Pages */
import { HomePage } from "./pages/Home";

export const App = () => {
    return (
        <Router>
            <Route path="/" element={<HomePage />} />
        </Router>
    );
};