import {Main} from "../components/Main";
import { Filter } from '../components/Filter'
import { Head } from '../components/Head'
import { CurrentWeather } from "../components/CurrentWeather";
import { Forecast } from "../components/Forecast";

export const HomePage = () => {
    return (
        <Main>
            <Filter />
            <Head />
            <CurrentWeather />
            <Forecast />
        </Main>
    );
}